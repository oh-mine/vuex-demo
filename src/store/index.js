// import { createStore } from "vuex";
import { createStore } from "./vuex";

const user = {
  state: {
    count: 0,
  },
  getters: {
    getUserCount: (state) => state.count,
  },
  mutations: {
    increment(state, payload = 1) {
      state.count += payload;
    },
    userIncrement(state, payload = 1) {
      state.count += payload;
    },
  },
  actions: {
    asyncIncrement({ commit }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit("increment", payload);
          resolve();
        }, 1000);
      });
    },
    asyncUserIncrement({ commit }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit("userIncrement", payload);
          resolve();
        }, 1000);
      });
    },
  },
  modules: {},
};

const home = {
  state: {
    count: 0,
  },
  getters: {
    getHomeCount: (state) => state.count,
  },
  mutations: {
    increment(state, payload = 1) {
      state.count += payload;
      console.log(state);
    },
    homeIncrement(state, payload = 1) {
      state.count += payload;
      console.log(state);
    },
  },
  actions: {
    asyncIncrement({ commit }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit("increment", payload);
          resolve();
        }, 1000);
      });
    },
    asyncHomeIncrement({ commit }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit("homeIncrement", payload);
          resolve();
        }, 1000);
      });
    },
  },
  modules: { user, info: {} },
};

const store = createStore({
  state: {
    count: 0,
  },
  getters: {
    getCount: (state) => state.count,
  },
  mutations: {
    increment(state, payload = 1) {
      state.count += payload;
      console.log(state);
    },
  },
  actions: {
    asyncIncrement({ commit }, payload) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit("increment", payload);
          resolve();
        }, 1000);
      });
    },
  },
  modules: { home, test: {} },
});

export default store;
