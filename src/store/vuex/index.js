import { reactive } from "vue";

export function createStore(options) {
  return new Store(options);
}

class ModuleCollection {
  constructor(rootModule) {
    this.register([], rootModule);
  }

  register(arr, rootModule) {
    let module = {
      _raw: rootModule,
      _state: reactive(rootModule.state || {}),
      _children: {},
    };

    if (arr.length === 0) {
      this.root = module;
    } else {
      /**
       * 1. ["home"] => splice => [].reduce => this.root._children["home"] = module
       * 2. ["home", "user"] => splice => ["home"].reduce => this.home._children["user"] = module
       */
      let parent = arr.splice(0, arr.length - 1).reduce((root, currentKey) => {
        return root._children[currentKey];
      }, this.root);
      parent._children[arr[arr.length - 1]] = module;
    }

    for (let childrenModuleName in rootModule.modules) {
      const childrenModule = rootModule.modules[childrenModuleName];
      this.register([...arr, childrenModuleName], {
        ...childrenModule,
        state: reactive(childrenModule.state || {}),
      });
    }
  }
}

export class Store {
  constructor(options = {}) {
    this.state = reactive(options.state);
    this.modules = new ModuleCollection(options);

    this.initModules([], this.modules.root);
  }

  install(app) {
    app.config.globalProperties.$store = this;
  }

  commit = (type, payload) => {
    this.mutations[type].forEach((fn) => {
      console.log(fn);
      fn(payload);
    });
  };

  dispatch = (type, payload) => {
    this.actions[type].forEach((fn) => fn(payload));
  };

  initGetters(options) {
    this.getters = this.getters || {};
    let getterList = options.getters || {};
    for (let key in getterList) {
      Object.defineProperty(this.getters, key, {
        get: () => {
          return getterList[key](options.state);
        },
      });
    }
  }

  initMutations(options) {
    this.mutations = this.mutations || {};
    let mutationList = options.mutations || {};
    for (let key in mutationList) {
      /**
       * mutations = {
       *   key: []
       * }
       */
      // mutations[key]已存在
      this.mutations[key] = this.mutations[key] || [];
      this.mutations[key].push((payload) => {
        console.log(options);
        mutationList[key](options.state, payload);
      });
    }
  }

  initActions(options) {
    this.actions = this.actions || {};
    let actionList = options.actions || {};
    for (let key in actionList) {
      this.actions[key] = this.actions[key] || [];
      this.actions[key].push((payload) => {
        actionList[key](this, payload);
      });
    }
  }

  initModules(arr, rootModule) {
    if (arr.length > 0) {
      let parent = arr.splice(0, arr.length - 1).reduce((state, currentKey) => {
        return state[currentKey];
      }, this.state);
      parent[arr[arr.length - 1]] = rootModule._state;
    }

    this.initGetters(rootModule._raw);
    this.initMutations(rootModule._raw);
    this.initActions(rootModule._raw);

    for (let childrenModuleName in rootModule._children) {
      let childrenModule = rootModule._children[childrenModuleName];
      this.initModules([...arr, childrenModuleName], childrenModule);
    }
  }
}
